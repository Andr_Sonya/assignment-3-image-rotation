#define _CRT_SECURE_NO_WARNINGS

#include "rotateImage.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char* argv[]) {

	if (argc != 4) {
		fprintf(stderr, "Not enough arguments");
		return 1;
	}

	const char* sourceImage = argv[1];
	const char* transformedImage = argv[2];
	int angle = atoi(argv[3]);

	char* current = argv[3];
	if (*current == '-' || *current == '+') {
		current++;
	}

	if (*current == '\0') {
		return 1;
	}

	while (*current != '\0') {
		if (*current < '0' || '9' < *current) {
			return 1;
		}
		current++;
	}


	rotateImage(sourceImage, transformedImage, angle);

	return 0;
}
