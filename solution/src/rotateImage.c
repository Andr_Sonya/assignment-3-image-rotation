#define _CRT_SECURE_NO_WARNINGS

#include "writetoFile.h"
#include "readfromFile.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void rotateImage(const char* sourceImage, const char* transformedImage, int angle) {

	BMPHeader header;
	BMPInfoHeader infoHeader;
	// Open the source image file
	FILE* sourceFile = fopen(sourceImage, "rb");
	if (sourceFile == NULL) {
		printf("Failed to open the source image file.\n");
		return;
	}

	// Read the BMP header
	size_t count = fread(&header, sizeof(BMPHeader), 1, sourceFile);
	if (count != sizeof(BMPHeader)) {
		if (feof(sourceFile)) {
			fprintf(stderr, "Wrong size.\n");
			fclose(sourceFile);
			return;
		}
		else if (ferror(sourceFile)) {
			fprintf(stderr, "Failed while reading file.\n");
			fclose(sourceFile);
			return;
		}
	}
	//
	if (header.signature[0] != 'B' || header.signature[1] != 'M') {
		printf("Invalid BMP file.\n");
		fclose(sourceFile);
		return;
	}


	// Read the BMP info header
	count = fread(&infoHeader, sizeof(BMPInfoHeader), 1, sourceFile);
	if (count != sizeof(BMPInfoHeader)) {
		if (feof(sourceFile)) {
			fprintf(stderr, "Wrong size.\n");
		}
		else if (ferror(sourceFile)) {
			fprintf(stderr, "Failed while reading file.\n");
		}
	}


	// Calculate the rotated dimensions
	int newWidth, newHeight;
	if (angle == 90 || angle == -90 || angle == 270 || angle == -270) {
		newWidth = infoHeader.height;
		newHeight = infoHeader.width;
	}
	else {
		newWidth = infoHeader.width;
		newHeight = infoHeader.height;
	}

	// Check if the image can be rotated
	const int validAngles[] = { 0, 90, -90, 180, -180, 270, -270 };
	int valid = 0;
	for (size_t i = 0; i < sizeof(validAngles) / sizeof(validAngles[0]); i++) {
		if (angle == validAngles[i]) {
			valid = 1;
			break;
		}
	}
	if (!valid) {
		printf("Invalid rotation angle.\n");
		fclose(sourceFile);
		return;
	}

	// Create the transformed image buffer
	char* transformedData = (char*)malloc(newWidth * newHeight * 3); // Assuming 24 bits per pixel (RGB)
	if (transformedData == NULL) {
		fprintf(stderr, "Failed to allocate memory for the transformed image.\n");
		fclose(sourceFile);
		return;
	}

	char* sourceData = (char*)malloc(newWidth * newHeight * 3);
	if (sourceData == NULL) {
		free(transformedData);
		fprintf(stderr, "Failed to allocate memory for the source image.\n");
		fclose(sourceFile);
		return;
	}

	readfromFile(sourceFile, sourceData, &infoHeader);

	// Rotate the image
	int x, y;
	int newX, newY;
	int index = 0;
	int readindex = 0;
	for (y = 0; y < infoHeader.height; y++) {
		for (x = 0; x < infoHeader.width; x++) {
			// Calculate the rotated coordinates
			if (angle == 90 || angle == -270) {
				newX = y;
				newY = infoHeader.width - x - 1;
			}
			else if (angle == -90 || angle == 270) {
				newX = infoHeader.height - y - 1;
				newY = x;
			}
			else if (angle == 180 || angle == -180) {
				newX = infoHeader.width - x - 1;
				newY = infoHeader.height - y - 1;
			}
			else {
				newX = x;
				newY = y;
			}

			readindex = (y * infoHeader.width + x) * 3;
			index = (newY * newWidth + newX) * 3;
			memcpy(transformedData + index, sourceData + readindex, 3); // NOLINT

		}
	}

	// Update the BMP info header with the rotated dimensions
	infoHeader.width = newWidth;
	infoHeader.height = newHeight;

	writetoFile(&header, &infoHeader, transformedData, transformedImage);

	// Close the files and free memory
	free(transformedData);
	free(sourceData);

	printf("Image rotated successfully.\n");
}
