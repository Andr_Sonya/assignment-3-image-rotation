#define _CRT_SECURE_NO_WARNINGS

#include "writetoFile.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int writetoFile(BMPHeader* h1, BMPInfoHeader* h2, char* data, const char* filename) {
	FILE* transformedFile = fopen(filename, "wb");
	if (transformedFile == NULL) {
		fprintf(stderr, "Failed to create the transformed image file.\n");
		return -1;
	}

	int padding = 0;
	if ((h2->width * 3) % 4) {
		padding = 4 - (h2->width * 3) % 4;
	}
	h2->imageSize = h2->height * (h2->width*3 + padding);
	h1->fileSize = h2->imageSize + sizeof(BMPHeader) + sizeof(BMPInfoHeader);
	fwrite(h1, sizeof(BMPHeader), 1, transformedFile);
	fwrite(h2, sizeof(BMPInfoHeader), 1, transformedFile);
	
	for (size_t i = 0; i < h2->height; i++) {
		const size_t write_n = h2->width * 3;
		size_t count = fwrite(data, sizeof(char), write_n, transformedFile);
		if (count != write_n) {
			if (feof(transformedFile)) {
				fprintf(stderr, "Wrong size.\n");
			}
			else if (ferror(transformedFile)) {
				fprintf(stderr, "Failed while reading file.\n");
			}
		}
		data += write_n;

		for (size_t k = 0; k < padding; k++) {
			fputc(0xcc, transformedFile);
		}
	}

	fclose(transformedFile);

	return 0;
}
