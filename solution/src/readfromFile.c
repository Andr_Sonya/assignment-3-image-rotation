#define _CRT_SECURE_NO_WARNINGS

#include "writetoFile.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void readfromFile(FILE* sourceFile, char* aa, BMPInfoHeader* infoHeader) {

	int padding = 0;
	if ((infoHeader->width * 3) % 4) {
		padding = 4 - (infoHeader->width * 3) % 4;
	}
	for (size_t i = 0; i < infoHeader->height; i++) {
		const size_t read_n = infoHeader->width * 3;
		size_t count = fread(aa, sizeof(char), read_n, sourceFile);
		if (count != sizeof(char)) {
			if (feof(sourceFile)) {
				fprintf(stderr, "Wrong size.\n");
			}
			else if (ferror(sourceFile)) {
				fprintf(stderr, "Failed while reading file.\n");
			}
		}
		
		aa += read_n;

		count = fseek(sourceFile, padding, SEEK_CUR);
		if (count != 0) {
			fprintf(stderr, "Failed with error.\n");
		}
	}

	fclose(sourceFile);
}
