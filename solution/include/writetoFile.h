#ifndef WRITE_H
#define WRITE_H

#pragma pack(push, 1)

typedef struct {
	unsigned char signature[2];
	unsigned int fileSize;
	unsigned short reserved1;
	unsigned short reserved2;
	unsigned int dataOffset;
} BMPHeader;

typedef struct {
	unsigned int headerSize;
	int width;
	int height;
	unsigned short planes;
	unsigned short bitsPerPixel;
	unsigned int compression;
	unsigned int imageSize;
	int xPixelsPerMeter;
	int yPixelsPerMeter;
	unsigned int colorsUsed;
	unsigned int colorsImportant;
} BMPInfoHeader;

#pragma pack(pop)

int writetoFile(BMPHeader* h1, BMPInfoHeader* h2, char* data, const char* filename);

#endif
