#ifndef ROTATE_H
#define ROTATE_H

void rotateImage(const char* sourceImage, const char* transformedImage, int angle);

#endif
